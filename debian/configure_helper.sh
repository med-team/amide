#!/bin/sh

## autoreconf --force --install ## this fails because there is no way to specify search path of m4 files for aclocal
intltoolize --copy --force --automake
aclocal -I m4  # specify where to seek for m4 files
libtoolize -f -c
automake -f -a -c
gtkdocize --copy
aclocal -I m4  --install 
autoconf -f
